﻿using System;
using System.Reflection;
using System.Windows.Forms;

namespace Sphere.QA.Tools.UnitDriver
{
    public partial class HelpForm : Form
    {
        public HelpForm()
        {
            InitializeComponent();
        }

        private void HelpForm_Load(object sender, EventArgs e)
        {
            Text += $" {Assembly.GetExecutingAssembly().GetName().Version}";
            LabelHelpText.Text = Resource.TextHelp;
        }
    }
}
