﻿using System.ComponentModel;
using Sphere.QA.Tools.UnitDriver.Objects.Controls;

namespace Sphere.QA.Tools.UnitDriver
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MenuMain = new System.Windows.Forms.MenuStrip();
            this.MenuMainFile = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMainFileOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMainHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMainHelpInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuMainChangeStyle = new System.Windows.Forms.ToolStripMenuItem();
            this.GroupBoxUpdateAuto = new System.Windows.Forms.GroupBox();
            this.ButtonUpdateAuto = new System.Windows.Forms.Button();
            this.TextBoxUpdateInterval = new System.Windows.Forms.TextBox();
            this.LabelUpdateInterval = new System.Windows.Forms.Label();
            this.Timer = new System.Windows.Forms.Timer(this.components);
            this.GroupBoxLog = new System.Windows.Forms.GroupBox();
            this.RichTextBoxLog = new Sphere.QA.Tools.UnitDriver.Objects.Controls.CustomRichTextBox();
            this.ContextMenuRichTextBox = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ContextMenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.ContextMenuClear = new System.Windows.Forms.ToolStripMenuItem();
            this.GroupBoxUpdateManual = new System.Windows.Forms.GroupBox();
            this.TextBoxVerticalAccuracy = new System.Windows.Forms.TextBox();
            this.LabelVerticalAccuracy = new System.Windows.Forms.Label();
            this.TextBoxHorizontalAccuracy = new System.Windows.Forms.TextBox();
            this.LabelHorizontalAccuracy = new System.Windows.Forms.Label();
            this.TextBoxSpeed = new System.Windows.Forms.TextBox();
            this.LabelSpeed = new System.Windows.Forms.Label();
            this.TextBoxCourse = new System.Windows.Forms.TextBox();
            this.LabelCourse = new System.Windows.Forms.Label();
            this.TextBoxAltitude = new System.Windows.Forms.TextBox();
            this.LabelAltitude = new System.Windows.Forms.Label();
            this.ButtonUpdateManual = new System.Windows.Forms.Button();
            this.TextBoxLongitude = new System.Windows.Forms.TextBox();
            this.TextBoxLatitude = new System.Windows.Forms.TextBox();
            this.TextBoxUnitId = new System.Windows.Forms.TextBox();
            this.LabelLongitude = new System.Windows.Forms.Label();
            this.LabelLatitude = new System.Windows.Forms.Label();
            this.LabelUnitId = new System.Windows.Forms.Label();
            this.MenuMain.SuspendLayout();
            this.GroupBoxUpdateAuto.SuspendLayout();
            this.GroupBoxLog.SuspendLayout();
            this.ContextMenuRichTextBox.SuspendLayout();
            this.GroupBoxUpdateManual.SuspendLayout();
            this.SuspendLayout();
            // 
            // MenuMain
            // 
            this.MenuMain.BackColor = System.Drawing.Color.Silver;
            this.MenuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuMainFile,
            this.MenuMainHelp});
            this.MenuMain.Location = new System.Drawing.Point(0, 0);
            this.MenuMain.Name = "MenuMain";
            this.MenuMain.Size = new System.Drawing.Size(900, 24);
            this.MenuMain.TabIndex = 0;
            this.MenuMain.Text = "MainMenu";
            // 
            // MenuMainFile
            // 
            this.MenuMainFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuMainFileOpen});
            this.MenuMainFile.Name = "MenuMainFile";
            this.MenuMainFile.Size = new System.Drawing.Size(48, 20);
            this.MenuMainFile.Text = "Файл";
            // 
            // MenuMainFileOpen
            // 
            this.MenuMainFileOpen.Name = "MenuMainFileOpen";
            this.MenuMainFileOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.MenuMainFileOpen.Size = new System.Drawing.Size(203, 22);
            this.MenuMainFileOpen.Text = "Загрузить файл";
            this.MenuMainFileOpen.Click += new System.EventHandler(this.MenuMainFileOpen_Click);
            // 
            // MenuMainHelp
            // 
            this.MenuMainHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuMainHelpInfo,
            this.MenuMainChangeStyle});
            this.MenuMainHelp.Name = "MenuMainHelp";
            this.MenuMainHelp.Size = new System.Drawing.Size(57, 20);
            this.MenuMainHelp.Text = "Разное";
            // 
            // MenuMainHelpInfo
            // 
            this.MenuMainHelpInfo.Name = "MenuMainHelpInfo";
            this.MenuMainHelpInfo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.MenuMainHelpInfo.Size = new System.Drawing.Size(226, 22);
            this.MenuMainHelpInfo.Text = "Справка";
            this.MenuMainHelpInfo.Click += new System.EventHandler(this.MenuMainHelpInfo_Click);
            // 
            // MenuMainChangeStyle
            // 
            this.MenuMainChangeStyle.CheckOnClick = true;
            this.MenuMainChangeStyle.Name = "MenuMainChangeStyle";
            this.MenuMainChangeStyle.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Space)));
            this.MenuMainChangeStyle.Size = new System.Drawing.Size(226, 22);
            this.MenuMainChangeStyle.Text = "Режим \"Сфера\"";
            this.MenuMainChangeStyle.Click += new System.EventHandler(this.MenuMainChangeStyle_Click);
            // 
            // GroupBoxUpdateAuto
            // 
            this.GroupBoxUpdateAuto.Controls.Add(this.ButtonUpdateAuto);
            this.GroupBoxUpdateAuto.Controls.Add(this.TextBoxUpdateInterval);
            this.GroupBoxUpdateAuto.Controls.Add(this.LabelUpdateInterval);
            this.GroupBoxUpdateAuto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GroupBoxUpdateAuto.Location = new System.Drawing.Point(12, 27);
            this.GroupBoxUpdateAuto.Name = "GroupBoxUpdateAuto";
            this.GroupBoxUpdateAuto.Size = new System.Drawing.Size(407, 81);
            this.GroupBoxUpdateAuto.TabIndex = 1;
            this.GroupBoxUpdateAuto.TabStop = false;
            this.GroupBoxUpdateAuto.Text = "Автоматический режим";
            // 
            // ButtonUpdateAuto
            // 
            this.ButtonUpdateAuto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonUpdateAuto.Location = new System.Drawing.Point(13, 45);
            this.ButtonUpdateAuto.Name = "ButtonUpdateAuto";
            this.ButtonUpdateAuto.Size = new System.Drawing.Size(301, 30);
            this.ButtonUpdateAuto.TabIndex = 2;
            this.ButtonUpdateAuto.Text = "Запустить автоматическое обновление";
            this.ButtonUpdateAuto.UseVisualStyleBackColor = true;
            this.ButtonUpdateAuto.Click += new System.EventHandler(this.ButtonUpdateAuto_Click);
            // 
            // TextBoxUpdateInterval
            // 
            this.TextBoxUpdateInterval.Location = new System.Drawing.Point(184, 17);
            this.TextBoxUpdateInterval.Name = "TextBoxUpdateInterval";
            this.TextBoxUpdateInterval.Size = new System.Drawing.Size(93, 21);
            this.TextBoxUpdateInterval.TabIndex = 1;
            // 
            // LabelUpdateInterval
            // 
            this.LabelUpdateInterval.AutoSize = true;
            this.LabelUpdateInterval.Location = new System.Drawing.Point(7, 20);
            this.LabelUpdateInterval.Name = "LabelUpdateInterval";
            this.LabelUpdateInterval.Size = new System.Drawing.Size(171, 15);
            this.LabelUpdateInterval.TabIndex = 0;
            this.LabelUpdateInterval.Text = "Интервал обновления (сек.)";
            // 
            // Timer
            // 
            this.Timer.Tick += new System.EventHandler(this.Timer_Tick);
            // 
            // GroupBoxLog
            // 
            this.GroupBoxLog.Controls.Add(this.RichTextBoxLog);
            this.GroupBoxLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GroupBoxLog.Location = new System.Drawing.Point(425, 27);
            this.GroupBoxLog.Name = "GroupBoxLog";
            this.GroupBoxLog.Size = new System.Drawing.Size(465, 381);
            this.GroupBoxLog.TabIndex = 3;
            this.GroupBoxLog.TabStop = false;
            this.GroupBoxLog.Text = "Лог";
            // 
            // RichTextBoxLog
            // 
            this.RichTextBoxLog.BackColor = System.Drawing.Color.FloralWhite;
            this.RichTextBoxLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RichTextBoxLog.ContextMenuStrip = this.ContextMenuRichTextBox;
            this.RichTextBoxLog.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.RichTextBoxLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RichTextBoxLog.ForeColor = System.Drawing.SystemColors.WindowText;
            this.RichTextBoxLog.Location = new System.Drawing.Point(7, 17);
            this.RichTextBoxLog.Name = "RichTextBoxLog";
            this.RichTextBoxLog.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RichTextBoxLog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.RichTextBoxLog.Size = new System.Drawing.Size(452, 357);
            this.RichTextBoxLog.TabIndex = 0;
            this.RichTextBoxLog.TabStop = false;
            this.RichTextBoxLog.Text = "";
            this.RichTextBoxLog.TextChanged += new System.EventHandler(this.RichTextBoxLog_TextChanged);
            // 
            // ContextMenuRichTextBox
            // 
            this.ContextMenuRichTextBox.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ContextMenuCopy,
            this.ContextMenuClear});
            this.ContextMenuRichTextBox.Name = "ContextMenuRichTextBox";
            this.ContextMenuRichTextBox.Size = new System.Drawing.Size(147, 48);
            // 
            // ContextMenuCopy
            // 
            this.ContextMenuCopy.Name = "ContextMenuCopy";
            this.ContextMenuCopy.Size = new System.Drawing.Size(146, 22);
            this.ContextMenuCopy.Text = "Скопировать";
            this.ContextMenuCopy.Click += new System.EventHandler(this.ContextMenuCopy_Click);
            // 
            // ContextMenuClear
            // 
            this.ContextMenuClear.Name = "ContextMenuClear";
            this.ContextMenuClear.Size = new System.Drawing.Size(146, 22);
            this.ContextMenuClear.Text = "Очистить";
            this.ContextMenuClear.Click += new System.EventHandler(this.ContextMenuClear_Click);
            // 
            // GroupBoxUpdateManual
            // 
            this.GroupBoxUpdateManual.Controls.Add(this.TextBoxVerticalAccuracy);
            this.GroupBoxUpdateManual.Controls.Add(this.LabelVerticalAccuracy);
            this.GroupBoxUpdateManual.Controls.Add(this.TextBoxHorizontalAccuracy);
            this.GroupBoxUpdateManual.Controls.Add(this.LabelHorizontalAccuracy);
            this.GroupBoxUpdateManual.Controls.Add(this.TextBoxSpeed);
            this.GroupBoxUpdateManual.Controls.Add(this.LabelSpeed);
            this.GroupBoxUpdateManual.Controls.Add(this.TextBoxCourse);
            this.GroupBoxUpdateManual.Controls.Add(this.LabelCourse);
            this.GroupBoxUpdateManual.Controls.Add(this.TextBoxAltitude);
            this.GroupBoxUpdateManual.Controls.Add(this.LabelAltitude);
            this.GroupBoxUpdateManual.Controls.Add(this.ButtonUpdateManual);
            this.GroupBoxUpdateManual.Controls.Add(this.TextBoxLongitude);
            this.GroupBoxUpdateManual.Controls.Add(this.TextBoxLatitude);
            this.GroupBoxUpdateManual.Controls.Add(this.TextBoxUnitId);
            this.GroupBoxUpdateManual.Controls.Add(this.LabelLongitude);
            this.GroupBoxUpdateManual.Controls.Add(this.LabelLatitude);
            this.GroupBoxUpdateManual.Controls.Add(this.LabelUnitId);
            this.GroupBoxUpdateManual.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GroupBoxUpdateManual.Location = new System.Drawing.Point(12, 114);
            this.GroupBoxUpdateManual.Name = "GroupBoxUpdateManual";
            this.GroupBoxUpdateManual.Size = new System.Drawing.Size(407, 294);
            this.GroupBoxUpdateManual.TabIndex = 4;
            this.GroupBoxUpdateManual.TabStop = false;
            this.GroupBoxUpdateManual.Text = "Ручной режим";
            // 
            // TextBoxVerticalAccuracy
            // 
            this.TextBoxVerticalAccuracy.Location = new System.Drawing.Point(125, 217);
            this.TextBoxVerticalAccuracy.Name = "TextBoxVerticalAccuracy";
            this.TextBoxVerticalAccuracy.Size = new System.Drawing.Size(156, 21);
            this.TextBoxVerticalAccuracy.TabIndex = 16;
            // 
            // LabelVerticalAccuracy
            // 
            this.LabelVerticalAccuracy.AutoSize = true;
            this.LabelVerticalAccuracy.Location = new System.Drawing.Point(10, 220);
            this.LabelVerticalAccuracy.Name = "LabelVerticalAccuracy";
            this.LabelVerticalAccuracy.Size = new System.Drawing.Size(95, 15);
            this.LabelVerticalAccuracy.TabIndex = 15;
            this.LabelVerticalAccuracy.Text = "VerticalAccuracy";
            // 
            // TextBoxHorizontalAccuracy
            // 
            this.TextBoxHorizontalAccuracy.Location = new System.Drawing.Point(125, 187);
            this.TextBoxHorizontalAccuracy.Name = "TextBoxHorizontalAccuracy";
            this.TextBoxHorizontalAccuracy.Size = new System.Drawing.Size(156, 21);
            this.TextBoxHorizontalAccuracy.TabIndex = 14;
            // 
            // LabelHorizontalAccuracy
            // 
            this.LabelHorizontalAccuracy.AutoSize = true;
            this.LabelHorizontalAccuracy.Location = new System.Drawing.Point(10, 190);
            this.LabelHorizontalAccuracy.Name = "LabelHorizontalAccuracy";
            this.LabelHorizontalAccuracy.Size = new System.Drawing.Size(111, 15);
            this.LabelHorizontalAccuracy.TabIndex = 13;
            this.LabelHorizontalAccuracy.Text = "HorizontalAccuracy";
            // 
            // TextBoxSpeed
            // 
            this.TextBoxSpeed.Location = new System.Drawing.Point(125, 158);
            this.TextBoxSpeed.Name = "TextBoxSpeed";
            this.TextBoxSpeed.Size = new System.Drawing.Size(156, 21);
            this.TextBoxSpeed.TabIndex = 12;
            // 
            // LabelSpeed
            // 
            this.LabelSpeed.AutoSize = true;
            this.LabelSpeed.Location = new System.Drawing.Point(10, 161);
            this.LabelSpeed.Name = "LabelSpeed";
            this.LabelSpeed.Size = new System.Drawing.Size(43, 15);
            this.LabelSpeed.TabIndex = 11;
            this.LabelSpeed.Text = "Speed";
            // 
            // TextBoxCourse
            // 
            this.TextBoxCourse.Location = new System.Drawing.Point(125, 129);
            this.TextBoxCourse.Name = "TextBoxCourse";
            this.TextBoxCourse.Size = new System.Drawing.Size(156, 21);
            this.TextBoxCourse.TabIndex = 10;
            // 
            // LabelCourse
            // 
            this.LabelCourse.AutoSize = true;
            this.LabelCourse.Location = new System.Drawing.Point(10, 132);
            this.LabelCourse.Name = "LabelCourse";
            this.LabelCourse.Size = new System.Drawing.Size(46, 15);
            this.LabelCourse.TabIndex = 9;
            this.LabelCourse.Text = "Course";
            // 
            // TextBoxAltitude
            // 
            this.TextBoxAltitude.Location = new System.Drawing.Point(125, 102);
            this.TextBoxAltitude.Name = "TextBoxAltitude";
            this.TextBoxAltitude.Size = new System.Drawing.Size(156, 21);
            this.TextBoxAltitude.TabIndex = 8;
            // 
            // LabelAltitude
            // 
            this.LabelAltitude.AutoSize = true;
            this.LabelAltitude.Location = new System.Drawing.Point(10, 104);
            this.LabelAltitude.Name = "LabelAltitude";
            this.LabelAltitude.Size = new System.Drawing.Size(47, 15);
            this.LabelAltitude.TabIndex = 7;
            this.LabelAltitude.Text = "Altitude";
            // 
            // ButtonUpdateManual
            // 
            this.ButtonUpdateManual.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonUpdateManual.Location = new System.Drawing.Point(13, 257);
            this.ButtonUpdateManual.Name = "ButtonUpdateManual";
            this.ButtonUpdateManual.Size = new System.Drawing.Size(130, 30);
            this.ButtonUpdateManual.TabIndex = 6;
            this.ButtonUpdateManual.Text = "Обновить позицию";
            this.ButtonUpdateManual.UseVisualStyleBackColor = true;
            this.ButtonUpdateManual.Click += new System.EventHandler(this.ButtonUpdateManual_Click);
            // 
            // TextBoxLongitude
            // 
            this.TextBoxLongitude.Location = new System.Drawing.Point(125, 72);
            this.TextBoxLongitude.Name = "TextBoxLongitude";
            this.TextBoxLongitude.Size = new System.Drawing.Size(156, 21);
            this.TextBoxLongitude.TabIndex = 5;
            // 
            // TextBoxLatitude
            // 
            this.TextBoxLatitude.Location = new System.Drawing.Point(125, 45);
            this.TextBoxLatitude.Name = "TextBoxLatitude";
            this.TextBoxLatitude.Size = new System.Drawing.Size(156, 21);
            this.TextBoxLatitude.TabIndex = 4;
            // 
            // TextBoxUnitId
            // 
            this.TextBoxUnitId.Location = new System.Drawing.Point(125, 17);
            this.TextBoxUnitId.Name = "TextBoxUnitId";
            this.TextBoxUnitId.Size = new System.Drawing.Size(272, 21);
            this.TextBoxUnitId.TabIndex = 3;
            // 
            // LabelLongitude
            // 
            this.LabelLongitude.AutoSize = true;
            this.LabelLongitude.Location = new System.Drawing.Point(10, 75);
            this.LabelLongitude.Name = "LabelLongitude";
            this.LabelLongitude.Size = new System.Drawing.Size(62, 15);
            this.LabelLongitude.TabIndex = 2;
            this.LabelLongitude.Text = "Longitude";
            // 
            // LabelLatitude
            // 
            this.LabelLatitude.AutoSize = true;
            this.LabelLatitude.Location = new System.Drawing.Point(10, 48);
            this.LabelLatitude.Name = "LabelLatitude";
            this.LabelLatitude.Size = new System.Drawing.Size(51, 15);
            this.LabelLatitude.TabIndex = 1;
            this.LabelLatitude.Text = "Latitude";
            // 
            // LabelUnitId
            // 
            this.LabelUnitId.AutoSize = true;
            this.LabelUnitId.Location = new System.Drawing.Point(10, 20);
            this.LabelUnitId.Name = "LabelUnitId";
            this.LabelUnitId.Size = new System.Drawing.Size(39, 15);
            this.LabelUnitId.TabIndex = 0;
            this.LabelUnitId.Text = "UnitId";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(900, 416);
            this.Controls.Add(this.GroupBoxUpdateManual);
            this.Controls.Add(this.GroupBoxLog);
            this.Controls.Add(this.GroupBoxUpdateAuto);
            this.Controls.Add(this.MenuMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MenuMain;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Sphere Unit Driver";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MenuMain.ResumeLayout(false);
            this.MenuMain.PerformLayout();
            this.GroupBoxUpdateAuto.ResumeLayout(false);
            this.GroupBoxUpdateAuto.PerformLayout();
            this.GroupBoxLog.ResumeLayout(false);
            this.ContextMenuRichTextBox.ResumeLayout(false);
            this.GroupBoxUpdateManual.ResumeLayout(false);
            this.GroupBoxUpdateManual.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip MenuMain;
        private System.Windows.Forms.ToolStripMenuItem MenuMainFile;
        private System.Windows.Forms.ToolStripMenuItem MenuMainFileOpen;
        private System.Windows.Forms.GroupBox GroupBoxUpdateAuto;
        private System.Windows.Forms.Label LabelUpdateInterval;
        private System.Windows.Forms.TextBox TextBoxUpdateInterval;
        private System.Windows.Forms.Button ButtonUpdateAuto;
        private System.Windows.Forms.Timer Timer;
        private System.Windows.Forms.GroupBox GroupBoxLog;
        private CustomRichTextBox RichTextBoxLog;
        private System.Windows.Forms.GroupBox GroupBoxUpdateManual;
        private System.Windows.Forms.Label LabelLongitude;
        private System.Windows.Forms.Label LabelLatitude;
        private System.Windows.Forms.Label LabelUnitId;
        private System.Windows.Forms.Button ButtonUpdateManual;
        private System.Windows.Forms.TextBox TextBoxLongitude;
        private System.Windows.Forms.TextBox TextBoxLatitude;
        private System.Windows.Forms.TextBox TextBoxUnitId;
        private System.Windows.Forms.TextBox TextBoxSpeed;
        private System.Windows.Forms.Label LabelSpeed;
        private System.Windows.Forms.TextBox TextBoxCourse;
        private System.Windows.Forms.Label LabelCourse;
        private System.Windows.Forms.TextBox TextBoxAltitude;
        private System.Windows.Forms.Label LabelAltitude;
        private System.Windows.Forms.TextBox TextBoxVerticalAccuracy;
        private System.Windows.Forms.Label LabelVerticalAccuracy;
        private System.Windows.Forms.TextBox TextBoxHorizontalAccuracy;
        private System.Windows.Forms.Label LabelHorizontalAccuracy;
        private System.Windows.Forms.ToolStripMenuItem MenuMainHelp;
        private System.Windows.Forms.ToolStripMenuItem MenuMainHelpInfo;
        private System.Windows.Forms.ToolStripMenuItem MenuMainChangeStyle;
        private System.Windows.Forms.ContextMenuStrip ContextMenuRichTextBox;
        private System.Windows.Forms.ToolStripMenuItem ContextMenuCopy;
        private System.Windows.Forms.ToolStripMenuItem ContextMenuClear;
    }
}

