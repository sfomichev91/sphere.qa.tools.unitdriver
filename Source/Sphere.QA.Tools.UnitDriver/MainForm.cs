﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Sphere.QA.Tools.UnitDriver.Objects;
using Sphere.QA.Tools.UnitDriver.Objects.Common;
using Sphere.QA.Tools.UnitDriver.Objects.Entities;

namespace Sphere.QA.Tools.UnitDriver
{
    public partial class MainForm : Form
    {
        private List<Control> _manualControls;

        private List<UnitData> _unitData;

        private bool _isUpdating;

        private int _currentUnit;

        private int _currentGeoData;

        public MainForm()
        {
            InitializeComponent();
            InitializeControls();
        }

        private void ChangeStyle()
        {
            if (MenuMainChangeStyle.Checked)
            {
                BackgroundImage = Resource.BackgroundForm;

                ButtonUpdateManual.BackgroundImage = Resource.BackgroundButton;
                ButtonUpdateAuto.BackgroundImage = Resource.BackgroundButton;

                ButtonUpdateManual.ForeColor = Color.White;
                ButtonUpdateAuto.ForeColor = Color.White;

                GroupBoxUpdateManual.ForeColor = Color.White;
                GroupBoxUpdateAuto.ForeColor = Color.White;

                GroupBoxUpdateManual.BackColor = Color.Transparent;
                GroupBoxUpdateAuto.BackColor = Color.Transparent;

                GroupBoxLog.ForeColor = Color.White;
                GroupBoxLog.BackColor = Color.Transparent;

                RichTextBoxLog.ForeColor = Color.White;

                MenuMain.ForeColor = Color.White;
                MenuMain.BackColor = Color.Transparent;

                foreach (var control in Controls)
                {
                    if (control.GetType() != typeof(ToolStripMenuItem))
                    {
                        var c = (Control) control;
                        c.ForeColor = Color.White;
                        c.BackColor = Color.Transparent;
                    }
                }

                return;
            }

            BackgroundImage = null;

            ButtonUpdateManual.BackgroundImage = null;
            ButtonUpdateAuto.BackgroundImage = null;

            ButtonUpdateManual.ForeColor = Color.Black;
            ButtonUpdateAuto.ForeColor = Color.Black;

            GroupBoxUpdateManual.ForeColor = Color.Black;
            GroupBoxUpdateAuto.ForeColor = Color.Black;

            GroupBoxUpdateManual.BackColor = Color.FromName("Control");
            GroupBoxUpdateAuto.BackColor = Color.FromName("Control");

            GroupBoxLog.ForeColor = Color.Black;
            GroupBoxLog.BackColor = Color.FromName("Control");

            MenuMain.ForeColor = Color.Black;
            MenuMain.BackColor = Color.FromName("Silver");

            RichTextBoxLog.ForeColor = Color.Black;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            Text += $" {Assembly.GetExecutingAssembly().GetName().Version}";
        }

        private void InitializeControls()
        {
            _manualControls = new List<Control>();
            foreach (var control in GroupBoxUpdateManual.Controls)
            {
                if (control.GetType() != typeof(Button))
                {
                    _manualControls.Add((Control)control);
                }
            }
        }

        private void MenuMainFileOpen_Click(object sender, EventArgs e)
        {
            var dialog = new OpenFileDialog
            {
                Filter = "JSON files (*.json) | *.json",
                InitialDirectory = $"{AppDomain.CurrentDomain.BaseDirectory}GeoData"
            };

            var result = dialog.ShowDialog(this);
            if (result != DialogResult.OK)
            {
                return;
            }

            if (!dialog.CheckFileExists)
            {
                MessageBox.Show("Указанного файла не существует", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var filePath = dialog.FileName;
            try
            {
                using (var sr = new StreamReader(filePath))
                {
                    string content = sr.ReadToEnd();
                    _unitData = JsonConvert.DeserializeObject<List<UnitData>>(content);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка при чтении файла:\n{ex.Message}", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            WriteLog($"Загружено юнитов: {_unitData.Count}");
            WriteLog($"Загружено данных: {_unitData.Sum(r => r.GeoData.Count)}");
        }

        private void ButtonUpdateAuto_Click(object sender, EventArgs e)
        {
            if (_unitData == null)
            {
                MessageBox.Show("Данные для юнитов не загружены", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!_isUpdating)
            {
                int.TryParse(TextBoxUpdateInterval.Text, out int interval);
                if (interval <= 0)
                {
                    MessageBox.Show("Указан некорректный интервал обновления", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                TextBoxUpdateInterval.Enabled = false;
                ButtonUpdateAuto.Text = "Остановить автоматическое обновление";

                _currentUnit = 0;
                Timer.Start();
                Timer.Interval = interval * 1000;
            }

            if (_isUpdating)
            {
                ButtonUpdateAuto.Text = "Запустить автоматическое обновление";
                TextBoxUpdateInterval.Enabled = true;
                Timer.Stop();
            }

            _isUpdating = !_isUpdating;

            // JSON generator
            //var list = new List<Route>
            //{
            //    new Route
            //    {
            //        UnitId = Guid.NewGuid().ToString(),
            //        Routes = new List<RouteData>
            //        {
            //            new RouteData
            //            {
            //                Altitude = 206.06,
            //                Latitude = 55.360000,
            //                Longitude = 54.078400,
            //                Course = 93.00,
            //                Speed = 200.00,
            //                HorizontalAccuracy = 0,
            //                VerticallAccuracy = 0
            //            },
            //        }
            //    }
            //};

            //var json = JsonConvert.SerializeObject(list, Formatting.Indented);
            //File.WriteAllText("test.json", json);
        }

        private void ButtonUpdateManual_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TextBoxUnitId.Text))
            {
                MessageBox.Show("Поле UnitId является обязательным к заполнению", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!Guid.TryParse(TextBoxUnitId.Text, out Guid unitId))
            {
                MessageBox.Show("Некорректный формат значения поля UnitId", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var buildData = BuildManualData();
            if (!buildData.IsSuccess)
            {
                MessageBox.Show(buildData.ErrorMessage, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            foreach (var control in _manualControls)
            {
                control.Enabled = false;
            }
            
            Task.Factory.StartNew(() =>
            {
                using (var db = new DbContext(Config.ConnectionString))
                {
                    var result = db.UpdateGeoPoint(buildData.Builder, unitId.ToString());
                    if (result)
                    {
                        NotifyUnity(unitId.ToString());
                    }

                    if (result)
                    {
                        WriteLog($"Данные обновлены для UnitId: {unitId}");
                    }
                    else
                    {
                        WriteLog($"Данные не обновлены для UnitId: {unitId}", true);
                    }
                }
            }).ContinueWith(task =>
            {
                if (!task.IsCompleted)
                {
                    MessageBox.Show("Операция была завершена с ошибкой", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                foreach (var control in _manualControls)
                {
                    control.Invoke(new Action(() => { control.Enabled = true; }));
                }

                ButtonUpdateManual.Invoke(new Action(() => { ButtonUpdateManual.Enabled = true; }));
            });
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            using (var db = new DbContext(Config.ConnectionString))
            {
                var route = _unitData[_currentUnit].GeoData[_currentGeoData];

                var buildData = BuildAutomationData(route);
                if (!buildData.IsSuccess)
                {
                    MessageBox.Show(buildData.ErrorMessage, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var result = db.UpdateGeoPoint(buildData.Builder, _unitData[_currentUnit].UnitId);
                if (result)
                {
                    NotifyUnity(_unitData[_currentUnit].UnitId);
                }

                if (result)
                {
                    WriteLog($"Данные обновлены для UnitId: {_unitData[_currentUnit].UnitId}");
                }
                else
                {
                    WriteLog($"Данные не обновлены для UnitId: {_unitData[_currentUnit].UnitId}", true);
                }
            }

            if (_currentGeoData + 1 <= _unitData[_currentUnit].GeoData.Count - 1)
            {
                _currentGeoData++;
                return;
            }

            _currentGeoData = 0;
            if (_currentUnit + 1 > _unitData.Count - 1)
            {
                Timer.Stop();
                WriteLog("Обновление данных завершено");
                TextBoxUpdateInterval.Enabled = true;
                _isUpdating = false;
                ButtonUpdateAuto.Text = "Запустить автоматическое обновление";

                return;
            }

            _currentUnit++;
        }

        private void WriteLog(string message, bool isError = false)
        {
            string status = isError ? "ERROR" : "INFO";
            RichTextBoxLog.Invoke(new Action(() =>
            {
                RichTextBoxLog.AppendText($"{DateTime.Now} {status} {message}\n");
            }));
        }

        private BuildDataResult BuildManualData()
        {
            try
            {
                var builder = new QueryBuilder();
                builder.Update("[dbo].[sph_GeoPoints]");

                if (!string.IsNullOrWhiteSpace(TextBoxLatitude.Text))
                {
                    if (!double.TryParse(TextBoxLatitude.Text.Replace(".", ","), out double latitude))
                    {
                        return new BuildDataResult("Некорректный формат значения поля Latitude");
                    }

                    builder.Set("[Latitude] = @latitude", "@latitude", latitude);
                }

                if (!string.IsNullOrWhiteSpace(TextBoxLongitude.Text))
                {
                    if (!double.TryParse(TextBoxLongitude.Text.Replace(".", ","), out double longitude))
                    {
                        return new BuildDataResult("Некорректный формат значения поля Longitude");
                    }

                    builder.Set("[Longitude] = @longitude", "@longitude", longitude);
                }

                if (!string.IsNullOrWhiteSpace(TextBoxAltitude.Text))
                {
                    if (!double.TryParse(TextBoxAltitude.Text.Replace(".", ","), out double altitude))
                    {
                        return new BuildDataResult("Некорректный формат значения поля Altitude");
                    }

                    builder.Set("[Altitude] = @altitude", "@altitude", altitude);
                }

                if (!string.IsNullOrWhiteSpace(TextBoxCourse.Text))
                {
                    if (!double.TryParse(TextBoxCourse.Text.Replace(".", ","), out double course))
                    {
                        return new BuildDataResult("Некорректный формат значения поля Course");
                    }

                    builder.Set("[Course] = @course", "@course", course);
                }

                if (!string.IsNullOrWhiteSpace(TextBoxHorizontalAccuracy.Text))
                {
                    if (!int.TryParse(TextBoxHorizontalAccuracy.Text, out int horizontalAccuracy))
                    {
                        return new BuildDataResult("Некорректный формат значения поля HorizontalAccuracy");
                    }

                    builder.Set("[HorizontalAccuracy] = @horizontalAccuracy", "@horizontalAccuracy", horizontalAccuracy);
                }

                if (!string.IsNullOrWhiteSpace(TextBoxVerticalAccuracy.Text))
                {
                    if (!int.TryParse(TextBoxVerticalAccuracy.Text, out int verticalAccuracy))
                    {
                        return new BuildDataResult("Некорректный формат значения поля VerticalAccuracy");
                    }

                    builder.Set("[VerticalAccuracy] = @verticalAccuracy", "@verticalAccuracy", verticalAccuracy);
                }

                if (!string.IsNullOrWhiteSpace(TextBoxSpeed.Text))
                {
                    if (!double.TryParse(TextBoxSpeed.Text.Replace(".", ","), out double speed))
                    {
                        return new BuildDataResult("Некорректный формат значения поля Speed");
                    }

                    builder.Set("[Speed] = @speed", "@speed", speed);
                }

                builder.Where();
                builder.Add("[Id] = @id");

                return new BuildDataResult(true)
                {
                    Builder = builder
                };
            }
            catch (Exception ex)
            {
                return new BuildDataResult($"Ошибка при формировании SQL-запроса\n{ex}");
            }
        }

        private static BuildDataResult BuildAutomationData(UnitGeoData data)
        {
            try
            {
                var builder = new QueryBuilder();
                builder.Update("[dbo].[sph_GeoPoints]");

                if (data.Latitude.HasValue)
                {
                    builder.Set("[Latitude] = @latitude", "@latitude", data.Latitude.Value);
                }

                if (data.Longitude.HasValue)
                {
                    builder.Set("[Longitude] = @longitude", "@longitude", data.Longitude.Value);
                }

                if (data.Altitude.HasValue)
                {
                    builder.Set("[Altitude] = @altitude", "@altitude", data.Altitude.Value);
                }

                if (data.Course.HasValue)
                {
                    builder.Set("[Course] = @course", "@course", data.Course.Value);
                }

                if (data.HorizontalAccuracy.HasValue)
                {
                    builder.Set("[HorizontalAccuracy] = @horizontalAccuracy", "@horizontalAccuracy", data.HorizontalAccuracy.Value);
                }

                if (data.VerticallAccuracy.HasValue)
                {
                    builder.Set("[VerticalAccuracy] = @verticalAccuracy", "@verticalAccuracy", data.VerticallAccuracy.Value);
                }

                if (data.Speed.HasValue)
                {
                    builder.Set("[Speed] = @speed", "@speed", data.Speed.Value);
                }

                builder.Where();
                builder.Add("[Id] = @id");

                return new BuildDataResult(true)
                {
                    Builder = builder
                };
            }
            catch (Exception ex)
            {
                return new BuildDataResult($"Ошибка при формировании SQL-запроса\n{ex}");
            }
        }

        private void NotifyUnity(string unitId)
        {
            using (var client = new ServiceClient(Config.ApiServiceUrl + "api/NotifyService/NotifyUnitPositionChanged"))
            {
                try
                {
                    var response = client.Post(new StringContent($"\"{unitId}\"", Encoding.UTF8, "application/json"));
                    if (response.StatusCode.Equals(HttpStatusCode.OK))
                    {
                        WriteLog("Нотификация отправлена успешно");
                    }
                }
                catch (Exception ex)
                {
                    WriteLog($"Ошибка при отправке уведомления в Unity\n{ex}", true);
                }
            }
        }

        private void RichTextBoxLog_TextChanged(object sender, EventArgs e)
        {
            RichTextBoxLog.SelectionStart = RichTextBoxLog.Text.Length;
            RichTextBoxLog.ScrollToCaret();
        }

        private void MenuMainHelpInfo_Click(object sender, EventArgs e)
        {
            using (var dialog = new HelpForm())
            {
                dialog.ShowDialog(this);
            }
        }

        private void MenuMainChangeStyle_Click(object sender, EventArgs e)
        {
            ChangeStyle();
        }

        private void ContextMenuCopy_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(RichTextBoxLog.SelectedText))
            {
                Clipboard.SetText(RichTextBoxLog.SelectedText);
                return;
            }

            Clipboard.SetText(RichTextBoxLog.Text);
        }

        private void ContextMenuClear_Click(object sender, EventArgs e)
        {
            RichTextBoxLog.Text = string.Empty;
        }
    }
}
