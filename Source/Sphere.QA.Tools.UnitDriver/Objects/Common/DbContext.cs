﻿using System;
using System.Data.SqlClient;
using System.Linq;
using Dapper;

namespace Sphere.QA.Tools.UnitDriver.Objects.Common
{
    internal sealed class DbContext : IDisposable
    {
        private readonly SqlConnection _connection;

        public DbContext(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
        }

        public bool UpdateGeoPoint(QueryBuilder builder, string unitId)
        {
            if (string.IsNullOrWhiteSpace(unitId))
            {
                return false;
            }

            var queryBuilder = new QueryBuilder();
            queryBuilder.Select("TOP (1) [Id]", "[dbo].[sph_GeoPoints] (NOLOCK)");
            queryBuilder.Where();
            queryBuilder.Add("[UnitId] = @unitId", "@unitId", unitId);
            queryBuilder.OrderByDesc("[Date]");

            string queryGetLastGeoPointId = queryBuilder.Build();

            var id = _connection.Query<Guid>(queryGetLastGeoPointId, queryBuilder.GetParameters()).SingleOrDefault();
            if (id.Equals(Guid.Empty))
            {
                return false;
            }

            var parameters = builder.GetParameters();
            parameters.Add("@id", id);

            string query = builder.Build();
            _connection.Query(query, parameters);

            return true;
        }

        public void Dispose()
        {
            _connection?.Dispose();
        }
    }
}
