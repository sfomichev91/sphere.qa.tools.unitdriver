﻿using System.Collections.Generic;
using Dapper;

namespace Sphere.QA.Tools.UnitDriver.Objects.Common
{
    internal sealed class QueryBuilder
    {
        private string _sqlQuery;

        private string _rawParameters;

        private readonly DynamicParameters _dynamicParameters;

        private readonly List<string> _parameters;

        public QueryBuilder()
        {
            _dynamicParameters = new DynamicParameters();
            _parameters = new List<string>();
        }

        public void Select(string fields, string table)
        {
            _sqlQuery = $"SELECT {fields} FROM {table} ";
        }

        public void Update(string table)
        {
            _sqlQuery = $"UPDATE {table} SET ";
        }

        public void Where()
        {
            BuildParameters();
            _sqlQuery += _rawParameters + " WHERE ";
        }

        public void Set(string parameter)
        {
            _parameters.Add(parameter);
            BuildParameters();
        }

        public void Set(string parameter, string name, object value)
        {
            _parameters.Add(parameter);
            BuildParameters();
            _dynamicParameters.Add(name, value);
        }

        public void Add(string parameter)
        {
            _sqlQuery += parameter;
        }

        public void Add(string parameter, string name, object value)
        {
            _sqlQuery += parameter;
            _dynamicParameters.Add(name, value);
        }

        public string Build()
        {
            return _sqlQuery;
        }

        public DynamicParameters GetParameters()
        {
            return _dynamicParameters;
        }

        private void BuildParameters()
        {
            _rawParameters = string.Join(", ", _parameters);
        }

        public void OrderBy(string orderBy)
        {
            _sqlQuery += $" ORDER BY {orderBy} ASC ";
        }
        public void OrderByDesc(string orderBy)
        {
            _sqlQuery += $" ORDER BY {orderBy} DESC ";
        }
    }
}
