﻿using System;
using System.Net.Http;

namespace Sphere.QA.Tools.UnitDriver.Objects.Common
{
    /// <inheritdoc />
    /// <summary>
    /// Клиент для выполнения Http Request
    /// </summary>
    internal sealed class ServiceClient : IDisposable
    {
        private readonly HttpClient _client;

        /// <summary>
        /// Конструктор, принимающий эндпоинт сервиса
        /// </summary>
        /// <param name="serviceEndPoint">Эндпоинт сервиса</param>
        /// <param name="timeout">Таймаут в секундах (необязательный)</param>
        public ServiceClient(string serviceEndPoint, int? timeout = null)
        {
            if (string.IsNullOrWhiteSpace(serviceEndPoint))
            {
                throw new ArgumentNullException(nameof(serviceEndPoint));
            }

            _client = new HttpClient
            {
                BaseAddress = new Uri(serviceEndPoint)
            };

            if (timeout.HasValue)
            {
                _client.Timeout = TimeSpan.FromSeconds(timeout.Value);
            }
        }

        /// <summary>
        /// Выполняет POST запрос
        /// </summary>
        /// <param name="content">Параметры запроса</param>
        /// <returns>HttpResponseMessage</returns>
        public HttpResponseMessage Post(HttpContent content)
        {
            return _client.PostAsync(_client.BaseAddress, content).Result;
        }

        /// <summary>
        /// Выполняет POST запрос
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        public HttpResponseMessage Post()
        {
            return _client.PostAsync(_client.BaseAddress, null).Result;
        }

        /// <summary>
        /// Выполняет PUT запрос
        /// </summary>
        /// <param name="content">Параметры запроса</param>
        /// <returns>HttpResponseMessage</returns>
        public HttpResponseMessage Put(HttpContent content)
        {
            return _client.PutAsync(_client.BaseAddress, content).Result;
        }

        /// <summary>
        /// Выполняет GET запрос
        /// </summary>
        /// <returns>HttpResponseMessage</returns>
        public HttpResponseMessage Get()
        {
            return _client.GetAsync(_client.BaseAddress).Result;
        }

        /// <summary>
        /// Добавляет параметр в Header запроса
        /// </summary>
        /// <param name="name">Имя параметра</param>
        /// <param name="value">Значение параметра</param>
        public void AddHeaderData(string name, string value)
        {
            _client.DefaultRequestHeaders.Add(name, value);
        }

        public void Dispose()
        {
            _client?.Dispose();
        }
    }
}
