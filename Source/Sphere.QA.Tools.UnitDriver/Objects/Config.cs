﻿using System.Configuration;

namespace Sphere.QA.Tools.UnitDriver.Objects
{
    internal sealed class Config
    {
        public static string ConnectionString => ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        public static string ApiServiceUrl => ConfigurationManager.AppSettings["ApiServiceUrl"];
    }
}
