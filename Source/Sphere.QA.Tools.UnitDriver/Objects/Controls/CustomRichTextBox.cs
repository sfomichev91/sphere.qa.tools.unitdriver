﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Sphere.QA.Tools.UnitDriver.Objects.Controls
{
    public class CustomRichTextBox : RichTextBox
    {
        [DllImport("user32.dll")]
        private static extern int HideCaret(IntPtr hwnd);

        protected override CreateParams CreateParams
        {
            get
            {
                var param = base.CreateParams;
                param.ExStyle |= 0x20;

                return param;
            }
        }

        public CustomRichTextBox()
        {
            this.MouseDown += this.ReadOnlyRichTextBox_Mouse;
            this.MouseUp += this.ReadOnlyRichTextBox_Mouse;
            base.ReadOnly = true;
            base.TabStop = false;
            HideCaret(this.Handle);
        }

        protected override void OnGotFocus(EventArgs e)
        {
            HideCaret(this.Handle);
        }

        protected override void OnEnter(EventArgs e)
        {
            HideCaret(this.Handle);
        }

        [DefaultValue(true)]
        public new bool ReadOnly
        {
            get => true;
            set { }
        }

        private void ReadOnlyRichTextBox_Mouse(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            HideCaret(this.Handle);
        }
    }
}
