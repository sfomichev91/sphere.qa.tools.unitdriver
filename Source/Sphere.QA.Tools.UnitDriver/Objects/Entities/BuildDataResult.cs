﻿using Sphere.QA.Tools.UnitDriver.Objects.Common;

namespace Sphere.QA.Tools.UnitDriver.Objects.Entities
{
    internal sealed class BuildDataResult
    {
        public QueryBuilder Builder { get; set; }

        public string ErrorMessage { get; set; }

        public bool IsSuccess { get; set; }

        public BuildDataResult(bool isSuccess)
        {
            IsSuccess = isSuccess;
        }

        public BuildDataResult(string errorMessage)
        {
            IsSuccess = false;
            ErrorMessage = errorMessage;
        }
    }
}
