﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Sphere.QA.Tools.UnitDriver.Objects.Entities
{
    internal sealed class UnitData
    {
        [JsonProperty("unitId")]
        public string UnitId { get; set; }

        [JsonProperty("geodata")]
        public List<UnitGeoData> GeoData { get; set; } = new List<UnitGeoData>();
    }
}
