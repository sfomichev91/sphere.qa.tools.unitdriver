﻿using Newtonsoft.Json;

namespace Sphere.QA.Tools.UnitDriver.Objects.Entities
{
    internal sealed class UnitGeoData
    {
        [JsonProperty("altitude")]
        public double? Altitude { get; set; }

        [JsonProperty("course")]
        public double? Course { get; set; }

        [JsonProperty("horizontalAccuracy")]
        public int? HorizontalAccuracy { get; set; }

        [JsonProperty("latitude")]
        public double? Latitude { get; set; }

        [JsonProperty("longitude")]
        public double? Longitude { get; set; }

        [JsonProperty("speed")]
        public double? Speed { get; set; }

        [JsonProperty("verticallAccuracy")]
        public int? VerticallAccuracy { get; set; }
    }
}
