﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Sphere.QA.Tools.UnitDriver")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SphereLab")]
[assembly: AssemblyProduct("Sphere.QA.Tools.UnitDriver")]
[assembly: AssemblyCopyright("Copyright © SphereLab 2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("66f9f72f-06f8-408b-8e4f-1a51c0e64e5d")]
[assembly: AssemblyVersion("1.0.0.*")]
[assembly: AssemblyFileVersion("1.0.0.0")]
